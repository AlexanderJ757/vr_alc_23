using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
     public float startTime = 3.0f;
     public int score = 0;
     public float lifeTime = 1.0f;
     public int hitBlockScore = 10;
     public float missBlockLife = 0.1f;
     public float wrongBlockLife = 0.08f;
     public float lifeRengenRate = 0.1f;
     public float swordHitVelocityThreshold = 0.5f;
     public VelocityTracker leftSwordTracker;
     public VelocityTracker rightSwordTracker;
     

     //instance 
      public static GameManager instance;

     void awake()
     {
        instance = this;
     }

     public void Addscore()
     {
           score = hitBlockScore;
           GameUI.instance.UpdateScoreText();
     }

     public void MissBlock()
     {
          lifeTime -= missBlockLife;
     }

     public void HitWrongBlock()
     {
          lifeTime -= wrongBlockLife;
     }

     void update()
     {
          // increase lifetime over time
          lifeTime = Mathf.MoveTowards(lifeTime, 1.0f, lifeRengenRate * Time.deltaTime);
          
          if(lifeTime <= 0.0f)
               LoseGame();
          
          // update the lifetime bat
          GameUI.instance.UpdateLifetimeBar();

          // if lifetime is less than or equal to zero
          if(lifeTime <= 0.0f)
               LoseGame();
     }

     //called when song is over
     public void WinGame()
     {
          SceneManager.LoadScene(0);
     }

     // called when lifetime hits 0
     public void LoseGame()
     {
          SceneManager.LoadScene(0);
     }
}
