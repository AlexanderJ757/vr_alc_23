using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockColor
{
     Green,
    Red
}

public class Block : MonoBehaviour
{
    public BlockColor color;

    public GameObject brokenBlockLeft;
    public GameObject brokenBlockRight;
    public float brokenBlockForce;
    public float brokenBlockTorque;
    public float brokenBlockDestroyDelay;
  
    void OntriggerEnter(Collider other)
    {
        if(other.CompareTag("sword green"))
        {
            if(color == BlockColor.Green)
            {
                //add to score
                GameManager.instance.Addscore();
            }
            else
            {
                // subtract a life
                GameManager.instance.HitWrongBlock();
            }

            Hit();
            
            if(color == BlockColor.Green && GameManager.instance.leftSwordTracker.velocity.magnitude >= GameManager.instance.swordHitVelocityThreshold)
            {
                GameManager.instance.Addscore();
            }
            else
            {
                GameManager.instance.HitWrongBlock();
            }
        }

        else if(other.CompareTag("sword red"))
        {
            if(color == BlockColor.Red)
            {
                //add to score
                GameManager.instance.Addscore();
            }
            else
            {
                // subtract a life
                GameManager.instance.HitWrongBlock();
            }

            Hit();

            if(color == BlockColor.Red && GameManager.instance.rightSwordTracker.velocity.magnitude >= GameManager.instance.swordHitVelocityThreshold)
            {
                GameManager.instance.Addscore();
            }
            else
            {
                GameManager.instance.HitWrongBlock();
            }

     
        }
    }
   

    public void Hit()
    {
        // enable the broken pieces
        brokenBlockLeft.SetActive(true);
        brokenBlockRight.SetActive(true);

        // remove them as children
        brokenBlockLeft.transform.parent = null;
        brokenBlockRight.transform.parent = null;

        //add force to them
        Rigidbody leftRig = brokenBlockLeft.GetComponent<Rigidbody>();
        Rigidbody rightRig = brokenBlockRight.GetComponent<Rigidbody>();

        leftRig.AddForce(-transform.right * brokenBlockForce, ForceMode.Impulse);
        rightRig.AddForce(transform.right * brokenBlockForce, ForceMode.Impulse);

        //Add Torque to them
        leftRig.AddTorque(-transform.forward * brokenBlockTorque,ForceMode.Impulse);
        rightRig.AddTorque(transform.forward * brokenBlockTorque,ForceMode.Impulse);

        //Destroy the broken pieces after a few seconds
        Destroy(brokenBlockLeft, brokenBlockDestroyDelay);
        Destroy(brokenBlockRight, brokenBlockDestroyDelay);

        //Destroy the main block
        Destroy(gameObject);
    }
}
